const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.post('/submit', (req, res) => {
    const { name, email, id, phone } = req.body;
    const data = `Name: ${name}, Email: ${email}, ID: ${id}, Phone: ${phone}\n`;

    fs.appendFile('submissions.txt', data, (err) => {
        if (err) throw err;
        console.log('Data saved to submissions.txt');
    });

    res.send('Form submitted successfully!');
});

app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});
